import csv
import fastavro
import glob
import simplejson as json
import logging
import os
import sys
from kbc.env_handler import KBCEnvHandler

KEY_DEBUG = 'debug'
KEY_COLUMNS = 'columns'
KEY_RAW = 'raw_mode'
KEY_MASK = 'extension_mask'
MANDATORY_PARAMS = []

sys.tracebacklimit = 0

# Disable logical readers, which can incorrectly convert data
fastavro._read.LOGICAL_READERS['long-timestamp-millis'] = lambda d, w, r: d
fastavro._read.LOGICAL_READERS['long-timestamp-micros'] = lambda d, w, r: d
# fastavro._read.LOGICAL_READERS['int-date'] = lambda d, w, r: d
# fastavro._read.LOGICAL_READERS['bytes-decimal'] = lambda d, w, r: d
# fastavro._read.LOGICAL_READERS['fixed-decimal'] = lambda d, w, r: d
fastavro._read.LOGICAL_READERS['string-uuid'] = lambda d, w, r: d
fastavro._read.LOGICAL_READERS['int-time-millis'] = lambda d, w, r: d
fastavro._read.LOGICAL_READERS['long-time-micros'] = lambda d, w, r: d


class AvroProcessor(KBCEnvHandler):

    def __init__(self):

        super().__init__(MANDATORY_PARAMS, log_level='INFO')

        if self.cfg_params.get(KEY_DEBUG, False) is True:
            logger = logging.getLogger()
            logger.setLevel(level='DEBUG')

        self.files_in_path = os.path.join(self.data_path, 'in', 'files')
        self.files_out_path = os.path.join(self.data_path, 'out', 'files')
        self.param_columns = self.cfg_params.get(KEY_COLUMNS, [])
        self.param_mask = self.cfg_params.get(KEY_MASK, '*')
        self.param_raw = self.cfg_params.get(KEY_RAW, False)
        self.avro_files = self.get_avro_files()

    def get_avro_files(self):

        _pattern = os.path.join(self.files_in_path, self.param_mask)
        _avro_files = glob.glob(_pattern)

        _len_avro_files = len(_avro_files)
        if _len_avro_files == 0:
            logging.info("No AVRO files found.")

        else:
            logging.info(f"Processing {_len_avro_files} AVRO files.")
            logging.debug(f"List of files: {_avro_files}.")

        return _avro_files

    def get_schema_from_files(self):

        schema = []

        for avro_path in self.avro_files:
            try:
                with open(avro_path, 'rb') as avro_file:
                    _rdr = fastavro.reader(avro_file)
                    _schema = _rdr.writer_schema
                    schema += [c['name'] for c in _schema['fields']]

            except ValueError as e:
                logging.exception(f"Error reading AVRO file at path {avro_path} - {e}.")
                sys.exit(1)

        result_schema = []
        duplicate_columns = []
        for column in schema:
            if column not in result_schema:
                result_schema += [column]
            else:
                duplicate_columns += [column]

        if (_dups := len(duplicate_columns)) != 0:
            logging.warn(f"{_dups} duplicate columns detected. Duplicates: {list(set(result_schema))}.")

        return result_schema

    def process_simple_avro_files(self, avro_path, file_path, schema):

        writer = csv.DictWriter(open(file_path, 'w', newline=''), fieldnames=schema, restval='', extrasaction='ignore',
                                quotechar='\"', quoting=csv.QUOTE_ALL, lineterminator='\n')
        writer.writeheader()

        try:
            _rn = 0
            with open(avro_path, 'rb') as avro_file:
                _rdr = fastavro.reader(avro_file)

                for row in _rdr:
                    writer.writerow(row)
                    _rn += 1

                    if _rn % 100000 == 0:
                        logging.debug(f"Processed {_rn} rows.")

                # writer.writerows(_rdr)

            logging.info(f"Converted {avro_path} to csv. Number of rows: {_rn}")

        except ValueError as e:
            logging.exception(f"Error reading AVRO file at path {avro_path} - {e}.")
            sys.exit(1)

    def process_raw_mode(self, avro_path, file_path):

        writer = csv.DictWriter(open(file_path, 'w', newline=''), fieldnames=['data'],
                                quotechar='\"', quoting=csv.QUOTE_ALL, lineterminator='\n')
        writer.writeheader()

        try:
            with open(avro_path, 'rb') as avro_file:
                _rdr = fastavro.reader(avro_file)
                _rn = 0
                for row in _rdr:
                    writer.writerow({'data': json.dumps(row)})
                    _rn += 1

                    if _rn % 100000 == 0:
                        logging.debug(f"Processed {_rn} rows.")

            logging.info(f"Converted {avro_path} to csv. Number of rows: {_rn}")

        except ValueError as e:
            logging.exception(f"Error reading AVRO file at path {avro_path} - {e}.")
            sys.exit(1)

    def run(self):

        if self.param_columns == [] or isinstance(self.param_columns, list) is False:
            _schema = self.get_schema_from_files()

        else:
            _schema = self.param_columns

        for avro_path in self.avro_files:
            logging.debug(f"Processing file {avro_path}.")
            file_name = os.path.basename(avro_path)
            file_path = os.path.join(self.files_out_path, file_name + '.csv')

            if self.param_raw is True:
                self.process_raw_mode(avro_path, file_path)

            else:
                self.process_simple_avro_files(avro_path, file_path, _schema)


if __name__ == '__main__':
    c = AvroProcessor()
    c.run()
