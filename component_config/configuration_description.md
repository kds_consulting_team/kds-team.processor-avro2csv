The complete sample configuration can be found the [component repository](https://bitbucket.org/kds_consulting_team/kds-team.processor-avro2csv/src/master/component_config/sample-config/).

The processor processes all files in `/data/in/files` folder, which match specified extension mask and outputs them to `/data/out/files` folder.

The processor configuration accepts following parameters:

- `raw_mode`
    - **description:** A boolean value specifying whether the avro should be converted to a csv or a json-like csv. If set to `true`, processor will create a csv and store each row of the avro file as a json inside. If set to `false`, the processor will try flatten structure and produce a valid csv.
    - **type:** optional
    - **values:** `true` or `false`
- `columns`
    - **description:** Allows to specify columns which should be present in the resulting csv file. If any of the columns is not present in the avro file, it will be automatically added. If the parameter is not specified, schema is automatically determined from matched avro files. **This parameter is ignored, if `raw_mode` is enabled.**
    - **type:** optional
    - **values:** an array of columns, e.g. `["col1", "col2"]`
- `extension_mask`
    - **description:** Allows to specify a mask for extensions, which should be processed. The parameter follows a glob syntax and defaults to `*`, i.e. all files are processed.
    - **type:** optional

### Sample configurations

Processing avro files in raw format, where result is a flat file with one json per row of avro file.

```json
{
    "definition": {
        "component": "kds-team.processor-avro2csv"
    },
    "parameters": {
        "raw_mode": true
    }
}
```

Processing avro files without any schema enforcement.

```json
{
    "definition": {
        "component": "kds-team.processor-avro2csv"
    }
}
```

Processing avro files with enforced schema and extension mask for filtering only wanted extensions.

```json
{
    "definition": {
        "component": "kds-team.processor-avro2csv"
    },
    "parameters": {
        "columns": ["order_id", "order_name", "customer_id"],
        "extension_mask": "*.avro"
    }
}
```

### Sample use-case

An avro file `data.avro0` was download using from Azure Blob storage. The following processors configuration can be used to convert the avro file to a csv (using this processor), move the file to `./tables` folder (using a [processor for moving files](https://components.keboola.com/components/keboola.processor-move-files)) and create a manifest for the table (using [processor for creating manifests](https://components.keboola.com/components/keboola.processor-create-manifest)).

```json
{
    "parameters": {
        "file": {
            "file_name": "/data/data.avro0",
            "primary_key": [],
            "incremental": false,
            "storage": "data_from_blob"
        }
    },
    "processors": {
        "after": [
            {
                "definition": {
                    "component": "kds-team.processor-avro2csv"
                }
            },
            {
                "definition": {
                    "component": "keboola.processor-move-files"
                },
                "parameters": {
                    "direction": "tables",
                    "addCsvSuffix": true,
                    "folder": "data_from_blob"
                }
            },
            {
                "definition": {
                    "component": "keboola.processor-create-manifest"
                },
                "parameters": {
                    "delimiter": ",",
                    "enclosure": "\"",
                    "incremental": false,
                    "primary_key": [
                        "id"
                    ]
                }
            }
        ]
    }
}
```