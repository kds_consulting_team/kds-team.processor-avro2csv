# Avro2CSV processor

The avro2csv processor allows for conversion of binary AVRO files to flat files. 

[Avro](https://avro.apache.org/) is a row-oriented data serialization framework develop by Apache and allows for storing arbitrarily complex schemas. Conversion of such complex schemas to flat files may not be arbitrary and that's why the extractor also supports a `raw_mode`, where the schema is converted to a new-line delimited JSON, stored in a csv.

**The processor currently supports only avro files with attached schema.**

The complete sample configuration can be found the [component repository](https://bitbucket.org/kds_consulting_team/kds-team.processor-avro2csv/src/master/component_config/sample-config/).